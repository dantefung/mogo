package com.loser.ext;

/**
 * 比较类型
 *
 * @author loser
 * @date 2023/2/4 17:12
 */
public enum ECheckType {

    NE,
    NOTNULL,
    NOTNULL_NE

}
