package com.loser.core.constant;


/**
 * 排序类型
 *
 * @author loser
 * @date 2023/2/4 17:12
 */
public enum ESortType {

    ASC,
    DESC

}
