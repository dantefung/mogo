package com.loser.core.constant;


/**
 * 比较类型
 *
 * @author loser
 * @date 2023/2/4 17:12
 */
public enum ECompare {

    EQ,
    NE,
    LE,
    LT,
    GE,
    GT,
    BW,
    IN,
    NIN

}
