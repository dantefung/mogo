package com.loser.gen.biz;

import lombok.Data;

/**
 * @author loser
 * @date 2023-01-09  17:35
 */
@Data
public class MyCusConfig {

    private String author;
    private String module;
    private String targetClass;

}
