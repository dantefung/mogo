package com.loser.module.mogo.service;

import com.loser.core.sdk.MogoService;
import com.loser.module.mogo.entity.User;

/**
 * 测试数据 接口
 *
 * @author loser
 * @date 2023-02-05  13:58
 */
public interface UserService extends MogoService<User> {

}
