package com.loser.common.entity;


public interface IBizStatus {

    String getCode();

    String getMsg();

}
